# browserguides

## Further information

https://neo4j.com/developer/guide-create-neo4j-browser-guide/ 

## Hosting of browser guides

The guides can be hosted anywhere. For security reasons, Neo4j’s standard config is setup to accept files hosted on guides.neo4j.com and localhost. However, if you want to host the guide elsewhere, you will need to whitelist the other domain in your $NEO4J_HOME/conf/neo4j.conf along with the default whitelists, as shown below.

```
#comma-separated list of base-urls, or * for everything
browser.remote_content_hostname_whitelist=https://example.com,https://guides.neo4j.com,localhost
browser.remote_content_hostname_whitelist=https://aksz15.git-pages.thm.de
browser.remote_content_hostname_whitelist=*
```

```
Load the Data for the Exercises: :play https://aksz15.git-pages.thm.de/browserguides/LoadData.html
Graph Catalog: :play https://aksz15.git-pages.thm.de/browserguides/GraphCatalog.html
Memory Requirements: :play https://aksz15.git-pages.thm.de/browserguides/MemoryRequirements.html
Weakly Connected Components :play https://aksz15.git-pages.thm.de/browserguides/WeaklyConnectedComponents.html
Label Propagation: :play https://aksz15.git-pages.thm.de/browserguides/LabelPropagation.html
Louvain Modularity: :play https://aksz15.git-pages.thm.de/browserguides/LouvainModularity.html
Triangle Count: :play https://aksz15.git-pages.thm.de/browserguides/TriangleCount.html
Local Clustering Coefficient: :play https://aksz15.git-pages.thm.de/browserguides/LocalClusteringCoefficient.html
PageRank: :play https://aksz15.git-pages.thm.de/browserguides/PageRank.html
Betweenness Centrality: :play https://aksz15.git-pages.thm.de/browserguides/BetweennessCentrality.html
Node Similarity: :play https://aksz15.git-pages.thm.de/browserguides/NodeSimilarity.html
Practical application: :play https://aksz15.git-pages.thm.de/browserguides/PracticalApplication.html
```

## Load Data

```
// Vorher alles löschen 
CALL apoc.periodic.iterate('MATCH (n) RETURN n', 
'DETACH DELETE n', {batchSize:1000});
CALL apoc.schema.assert({},{},true) 
YIELD label, key RETURN *;

// Game of Thrones
CREATE CONSTRAINT ON (c:Character) ASSERT c.id IS UNIQUE;
UNWIND range(1,7) AS season
LOAD CSV WITH HEADERS FROM "https://github.com/neo4j-apps/neuler/raw/master/sample-data/got/got-s" + season + "-nodes.csv" AS row
MERGE (c:Character {id: row.Id})
ON CREATE SET c.name = row.Label;
UNWIND range(1,7) AS season
LOAD CSV WITH HEADERS FROM "https://github.com/neo4j-apps/neuler/raw/master/sample-data/got/got-s" + season + "-edges.csv" AS row
MATCH (source:Character {id: row.Source})
MATCH (target:Character {id: row.Target})
CALL apoc.merge.relationship(source, "INTERACTS_SEASON" + season, {}, {}, target) YIELD rel
SET rel.weight = toInteger(row.Weight);

// European Roads
CREATE CONSTRAINT ON (p:Place) ASSERT p.name IS UNIQUE;
LOAD CSV WITH HEADERS FROM "https://github.com/neo4j-apps/neuler/raw/master/sample-data/eroads/roads.csv"
AS row
MERGE (origin:Place {name: row.origin_reference_place})
SET origin.countryCode = row.origin_country_code
MERGE (destination:Place {name: row.destination_reference_place})
SET destination.countryCode = row.destination_country_code
MERGE (origin)-[eroad:EROAD {road_number: row.road_number}]->(destination)
SET eroad.distance = toInteger(row.distance), eroad.watercrossing = row.watercrossing;
MATCH (:Place)-[r:EROAD]->(:Place) SET r.inverse_distance = 1.0 / log10(r.distance + 2);

// Recipies
CREATE CONSTRAINT ON (r:Recipe) ASSERT r.name IS UNIQUE;
CREATE CONSTRAINT ON (i:Ingredient) ASSERT i.name IS UNIQUE;
LOAD CSV WITH HEADERS FROM "https://github.com/neo4j-apps/neuler/raw/master/sample-data/recipes/recipes.csv"
AS row
MERGE (r:Recipe{name:row.recipe})
WITH r,row.ingredients as ingredients
UNWIND split(ingredients,',') as ingredient
MERGE (i:Ingredient{name:ingredient})
MERGE (r)-[:CONTAINS_INGREDIENT]->(i);

// PersonsHelpsGraph
MERGE (nAlice:Person {name:'Alice'})
MERGE (nBridget:Person {name:'Bridget'})
MERGE (nCharles:Person {name:'Charles'})
MERGE (nDoug:Person {name:'Doug'})
MERGE (nMark:Person {name:'Mark'})
MERGE (nMichael:Person {name:'Michael'})
MERGE (nAlice)-[:HELPS{weight:5.0}]->(nBridget)
MERGE (nAlice)-[:HELPS{weight:1.5}]->(nCharles)
MERGE (nMark)-[:HELPS{weight:2.0}]->(nDoug)
MERGE (nMark)-[:HELPS{weight:1.7}]->(nMichael)
MERGE (nBridget)-[:HELPS{weight:0.5}]->(nMichael)
MERGE (nDoug)-[:HELPS{weight:1.1}]->(nMark)
MERGE (nMichael)-[:HELPS{weight:0.5}]->(nAlice)
MERGE (nAlice)-[:HELPS{weight:1.1}]->(nMichael)
MERGE (nBridget)-[:HELPS{weight:2.6}]->(nAlice)
MERGE (nMichael)-[:HELPS{weight:1.8}]->(nBridget);

CREATE CONSTRAINT airports IF NOT EXISTS ON (a:Airport) ASSERT a.iata IS UNIQUE;
CREATE CONSTRAINT cities IF NOT EXISTS ON (c:City) ASSERT c.name IS UNIQUE;
CREATE CONSTRAINT regions IF NOT EXISTS ON (r:Region) ASSERT r.name IS UNIQUE;
CREATE CONSTRAINT countries IF NOT EXISTS ON (c:Country) ASSERT c.code IS UNIQUE;
CREATE CONSTRAINT continents IF NOT EXISTS ON (c:Continent) ASSERT c.code IS UNIQUE;
CREATE INDEX locations IF NOT EXISTS FOR (air:Airport) ON (air.location);
WITH
    'https://raw.githubusercontent.com/neo4j-graph-examples/graph-data-science2/main/data/airport-node-list.csv'
    AS url
LOAD CSV WITH HEADERS FROM url AS row
MERGE (a:Airport {iata: row.iata})
MERGE (ci:City {name: row.city})
MERGE (r:Region {name: row.region})
MERGE (co:Country {code: row.country})
MERGE (con:Continent {name: row.continent})
MERGE (a)-[:IN_CITY]->(ci)
MERGE (a)-[:IN_COUNTRY]->(co)
MERGE (ci)-[:IN_COUNTRY]->(co)
MERGE (r)-[:IN_COUNTRY]->(co)
MERGE (a)-[:IN_REGION]->(r)
MERGE (ci)-[:IN_REGION]->(r)
MERGE (a)-[:ON_CONTINENT]->(con)
MERGE (ci)-[:ON_CONTINENT]->(con)
MERGE (co)-[:ON_CONTINENT]->(con)
MERGE (r)-[:ON_CONTINENT]->(con)
SET a.id = row.id,
    a.icao = row.icao,
    a.city = row.city,
    a.descr = row.descr,
    a.runways = toInteger(row.runways),
    a.longest = toInteger(row.longest),
    a.altitude = toInteger(row.altitude),
    a.location = point({latitude: toFloat(row.lat), longitude: toFloat(row.lon)});
LOAD CSV WITH HEADERS FROM 'https://raw.githubusercontent.com/neo4j-graph-examples/graph-data-science2/main/data/iroutes-edges.csv' AS row
MATCH (source:Airport {iata: row.src})
MATCH (target:Airport {iata: row.dest})
MERGE (source)-[r:HAS_ROUTE]->(target)
ON CREATE SET r.distance = toInteger(row.dist);
```
