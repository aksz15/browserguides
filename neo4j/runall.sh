./run.sh adoc/index.adoc html/index.html
./run.sh adoc/BetweennessCentrality.adoc html/BetweennessCentrality.html
./run.sh adoc/LoadData.adoc html/LoadData.html
./run.sh adoc/NodeSimilarity.adoc html/NodeSimilarity.html
./run.sh adoc/WeaklyConnectedComponents.adoc html/WeaklyConnectedComponents.html
./run.sh adoc/GraphCatalog.adoc html/GraphCatalog.html
./run.sh adoc/LocalClusteringCoefficient.adoc html/LocalClusteringCoefficient.html
./run.sh adoc/PageRank.adoc html/PageRank.html
./run.sh adoc/LouvainModularity.adoc html/LouvainModularity.html
./run.sh adoc/LabelPropagation.adoc html/LabelPropagation.html
./run.sh adoc/MemoryRequirements.adoc html/MemoryRequirements.html
./run.sh adoc/TriangleCount.adoc html/TriangleCount.html

